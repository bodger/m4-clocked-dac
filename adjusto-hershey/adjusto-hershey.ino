#include <Adafruit_ZeroTimer.h>
#include "chars.h"

#define BUFSIZE     1024                // size of each waveform buffer (in samples)
#define DACRES      (1 << 12)           // DAC resolution
#define MAXDAC      (DACRES - 1)        // max DAC value
#define HRES        (DACRES >> 1)       // half the resolution of the DAC

#define SWAP(x, y)          tmp = x; x = y; y = tmp

//#define DEBUGV
//#define DEBUGXY
//#define DEBUGSET
//#define DEBUGSTR

#ifdef DEBUGV || DEBUGXY || DEBUGSET || DEBUGSTR
#define DEBUG
#endif

//#define DEBUG

// drawing string structure

struct dstring {
  const char *  text;
  int           height;
  int           width;
};

// coördinate buffer structure

struct sbuf {
  uint16_t    x;
  uint16_t    y;
};

// position pair structure

struct pair {
  int v1;
  int v2;
};

static int                  getval(char codechar);
static void                 updatescale();
static void                 getpair(const char * code, int pos, struct pair * pptr);
static const struct hchar * lookup(const char ch);

static int                  freq;                   // current sample frequency
static int                  x;                      // current character X position
static int                  y;                      // current character Y position
static int                  drawx0;                 // line segment begin X
static int                  drawy0;                 // line segment begin Y
static int                  drawx1;                 // line segment end X
static int                  drawy1;                 // line segment end Y
static int                  dx;                     // delta X
static int                  dy;                     // delta Y
static int                  drawx;                  // current point X
static int                  drawy;                  // current point Y
static int                  error2;                 // error term
static int                  derror2;                // delta error term
static int                  pos;                    // Hershey code position
static int                  offset;                 // current character X offset
static int                  kern;                   // current character width
static int                  maxwidth;               // width of widest string
static bool                 pendown;                // next line segment pen down flag  
static bool                 steep;                  // line segment slope > 1 flag
static bool                 drawingline = false;    // currently drawing a line segment flag
static bool                 haslastpos = false;     // last position valid flag
static float                scale;                  // current scale factor
static float                xslope;                 // font to drawing transformation X slope
static float                yslope;                 // font to drawing transformation Y slope
static float                xb;                     // font to drawing transformation X intercept
static float                yb;                     // font to drawing transformation Y intercept
static float                sxslope;                // scaled X slope
static float                syslope;                // scaled Y slope
static float                sxb;                    // scaled X intercept
static float                syb;                    // scaled Y intercept
static struct pair          curpos;                 // current line segment end position
static struct pair          lastpos;                // line segment begin position
static const char *         strptr = NULL;          // pointer to character within string
static struct sbuf          wave[BUFSIZE];          // output wave circular buffer
static struct sbuf *        wptr = wave;            // output write pointer
static struct sbuf *        rptr = wave;            // output read pointer
static struct sbuf *        bend = wave + BUFSIZE;  // output buffer end pointer
static volatile bool        tick = false;           // output clock tick flag
static struct dstring *     textp;                  // text pointer
static struct dstring *     texte;                  // text end
static const struct hchar * hptr = NULL;            // Hershey glyph pointer

static Adafruit_ZeroTimer   zerotimer = Adafruit_ZeroTimer(3);

static struct dstring  text[] = {
  { "Heathkit", 0, 0 },
  { "HO-10", 0, 0 },
  { NULL, 0, 0 }
};

// set up AdaFruit ZeroTimer handler

void TC3_Handler() {
  Adafruit_ZeroTimer::timerHandler(3);
}

// timer callback, just set flag

void TimerCallback0(void)
{
  tick = true;
}

// update timer frequency

static void updatefreq()
{  
  zerotimer.enable(false);
  zerotimer.setCompare(0, freq);
  zerotimer.setCallback(true, TC_CALLBACK_CC_CHANNEL0, TimerCallback0);
  zerotimer.enable(true);
}

// get position pair from Hershey code

static void
getpair(
  const char *  code, // pointer to code values
  int           pos,  // code position to get
  struct pair * pptr) // pointer to receive position pair
{
  code += pos * 2;  // adjust code pointer for position
  pptr->v1 = getval(code[0]);
  pptr->v2 = getval(code[1]);
}

// get position value from Hershey code

static int
getval(
  char  codechar) // position code
{
  return(codechar - HERSHEYZERO);
}

// find glyph definition in array

static const struct hchar *
lookup(
  const char            ch)   // character to find
{
  const struct hchar *  hptr; // Hershey structure pointer

  for (hptr = hchars; hptr->glyph != '\0'; ++hptr) {
    if (hptr->glyph == ch) {
      return(hptr);
    }
  }

  // not found
  return(NULL);
}

static void
updatescale()
{
  sxslope = xslope / scale;
  syslope = yslope / scale;
  sxb     = xb / scale;
  syb     = yb / scale;
}

// check inputs to see if user wants to adjust the settings

static void checkinputs()
{
  int             newfreq;  // new sample frequency
  float           newscale; // new scale factor

  newfreq  = analogRead(A5) / 64 + 48;
  newscale = analogRead(A4) / 64.0;

#ifdef DEBUGV
  Serial.print("newfreq ");
  Serial.print(newfreq);
  Serial.print(" newscale ");
  Serial.println(newscale);
#endif

  if (abs(newfreq - freq) > 0x2) {
    // new frequency detected, update timer
#ifdef DEBUGSET
    Serial.print("new frequency ");
    Serial.println(newfreq);
#endif
    freq = newfreq;
    updatefreq();
  }

  if (abs(newscale - scale) > 1) {
#ifdef DEBUGSET
    Serial.print("new scale ");
    Serial.println(newscale);
#endif
    if (newscale != 0) {
      scale = newscale;
      updatescale();
    }
  }
}

// compute string width

static int stringwidth(
  const char *  string)
{
#ifdef DEBUG
  Serial.println("computing string width");
#endif
  x = 0;
  
 for (strptr = string; *strptr != '\0'; ++strptr) {
#ifdef DEBUG
    Serial.print("glyph ");
    Serial.println(*strptr);
#endif
    // look up glyph
    hptr = lookup(*strptr);

    // get glyph position data
    getpair(hptr->code, 0, &curpos);

    // add glyph width
    x += curpos.v2 - curpos.v1;
  }

   return x;
}

// no current character, load a string

static void setupstring()
{
  ++textp;

#ifdef DEBUGV
  Serial.print("textp ");
  Serial.print((unsigned) textp, 16);
  Serial.print(" text ");
  Serial.print(textp->text);
#endif

  if (textp->text == NULL) {
    textp = text;
#ifdef DEBUGV
    Serial.print("resetting, now text ");
    Serial.print(textp->text);
#endif
    y = 1000;
  } else {
    y += textp->height;
  }

  // this is a good time to check inputs
  checkinputs();

  // reset to beginning of text
  strptr = textp->text;
#ifdef DEBUGSTR
  Serial.println(strptr);
#endif
  x      = (maxwidth - textp->width) / 2;
}

static void setupglyph()
{
#ifdef DEBUGV
  Serial.println("no current glyph");
#endif
  // no current glyph, see if there is a current character
  if ((strptr == NULL) || (*strptr == '\0')) {
#ifdef DEBUGV
    Serial.println("no current char");
#endif
    setupstring();
  }

  // we have a current character, start glyph
#ifdef DEBUGSTR
  Serial.print("glyph ");
  Serial.println(*strptr);
#endif
  hptr       = lookup(*strptr);
  pos        = 0;
  pendown    = false;
  haslastpos = false;
}

static void getnextcmd()
{
#ifdef DEBUGV
  Serial.println("get next drawing command");
#endif

  if (pos >= hptr->npairs) {  // is position end of glyph?
#ifdef DEBUGV
    Serial.println("end of glyph");
#endif
    ++strptr;                 // yes, move to next character
    x += kern;                // advance X position by character width
    hptr = NULL;              // clear glyph pointer
  }

  if (hptr == NULL) {
    setupglyph();
  }
  
  // we have a current glyph and position, get code

#ifdef DEBUGV
  Serial.print("pos ");
  Serial.println(pos);
#endif
  getpair(hptr->code, pos++, &curpos);
#ifdef DEBUGV
  Serial.print("curpos ");
  Serial.print(curpos.v1);
  Serial.print(" ");
  Serial.println(curpos.v2);
#endif

  if (pos == 1) {
    // beginning of glyph, use position code to set start and next positions
    offset = x - curpos.v1;
    kern   = curpos.v2 - curpos.v1;
#ifdef DEBUGXY
    Serial.print("offset ");
    Serial.print(offset);
    Serial.print(" kern ");
    Serial.println(kern);
#endif
    return;
  }

  if (curpos.v1 == PENUP) {
    // pen up code, clear drawing flag
#ifdef DEBUGV
    Serial.println("pen up");
#endif
    pendown = false;
    haslastpos = false;
    return;
  }

  pendown = haslastpos;
  haslastpos = true;
}

static void setupline()
{
  int             tmp;      // temporary variable used for swapping

  while (!pendown) {
    lastpos.v1 = curpos.v1;
    lastpos.v2 = curpos.v2;
#ifdef DEBUGXY
    Serial.print("lastpos ");
    Serial.print(lastpos.v1);
    Serial.print(" ");
    Serial.println(lastpos.v2);
#endif
    getnextcmd();
  }
#ifdef DEBUGXY
  Serial.print("raw ");
  Serial.print(offset);
  Serial.print(" + ");
  Serial.print(lastpos.v1);
  Serial.print(", ");
  Serial.print(lastpos.v2);
  Serial.print(" - ");
  Serial.print(curpos.v1);
  Serial.print(", ");
  Serial.println(curpos.v2);
#endif

  // pen down, generate line segment from last position to current position
  drawx0      = (lastpos.v1 + offset) * sxslope + sxb;
  drawy0      = (lastpos.v2 + y     ) * syslope + syb;
  drawx1      = (curpos.v1  + offset) * sxslope + sxb;
  drawy1      = (curpos.v2  + y     ) * syslope + syb;
#ifdef DEBUGXY
  Serial.print("out ");
  Serial.print(scale);
  Serial.print(" * ");
  Serial.print(drawx0);
  Serial.print(",");
  Serial.print(drawy0);
  Serial.print(" - ");
  Serial.print(drawx1);
  Serial.print(",");
  Serial.println(drawy1);
#endif
  drawingline = true;
  steep       = false;

  if (abs(drawx0 - drawx1) < abs(drawy0 - drawy1)) {
    // slope more than 1, increment Y instead of X
    SWAP(drawx0, drawy0);
    SWAP(drawx1, drawy1);
    steep = true;
  }

  if (drawx0 > drawx1) {
    // right to left, swap points
    SWAP(drawx0, drawx1);
    SWAP(drawy0, drawy1);
  }

  dx      = drawx1 - drawx0;
  dy      = drawy1 - drawy0;
#ifdef DEBUGV
  Serial.print("dx ");
  Serial.print(dx);
  Serial.print(" dy ");
  Serial.println(dy);
#endif
  derror2 = abs(dy) * 2;
  error2  = 0;
  drawx   = drawx0;
  drawy   = drawy0;
}

// called once at beginning of execution

void
setup()
{
  int     totalheight;
#ifdef DEBUG
  uint8_t i;
#endif

//  pinMode(A0, OUTPUT);
//  pinMode(A1, OUTPUT);

#ifdef DEBUG
  while (!Serial) {
  }

  Serial.begin(9600);

  for (i = 0; i < 5; ++i) {
    Serial.println(i);

    delay(200);
  }

  Serial.println("init()");
#endif

  maxwidth = 0;
  totalheight = 0;
  textp = text;

  for (texte = text; texte->text != NULL; ++texte) {
    Serial.print("getting width for ");
    Serial.println(texte->text);
    texte->width = stringwidth(texte->text);
    Serial.print("got width ");
    Serial.println(texte->width);
    texte->height = 30;
    totalheight += texte->height;

    if (texte->width > maxwidth) {
      maxwidth = texte->width;
    }
  }

  // compute scale from string width and DAC range
  xslope = (float) MAXDAC / maxwidth;
  yslope = -xslope;
  xb     = MAXDAC;
  yb     = HRES / 2 - totalheight;

#ifdef DEBUG
  Serial.print("max width ");
  Serial.print(maxwidth);
  Serial.print(" slope ");
  Serial.println(xslope);
#endif

  freq  = 200;
  scale = 2;

  updatescale();

  zerotimer.enable(false);
  zerotimer.configure(TC_CLOCK_PRESCALER_DIV1,       // prescaler
          TC_COUNTER_SIZE_16BIT,       // bit width of timer/counter
          TC_WAVE_GENERATION_MATCH_PWM // frequency or PWM mode
          );

  zerotimer.setCompare(0, freq);
  zerotimer.setCallback(true, TC_CALLBACK_CC_CHANNEL0, TimerCallback0);
  zerotimer.enable(true);
}

// called repeatedly during execution

void
loop()
{
  struct sbuf *   tptr;     // temporary write pointer

  if (tick) {
    // time to update signal, check if data available
    if (rptr != wptr) {
      // data is available, update signal
      analogWrite(A0, rptr->x * scale);
      analogWrite(A1, rptr->y * scale);

      // increment read pointer, check for wraparound
      if (++rptr == bend) {
        // read pointer at end of buffer, wrap around
        rptr = wave;
      }

      // serviced current tick
      tick = false;
    }
  }

  // make temporary copy of new write pointer

  tptr = wptr + 1;

  if (tptr == bend) {
    tptr = wave;
  }

#ifdef DEBUGV
  Serial.print("read ");
  Serial.print((uint32_t) rptr, 16);
  Serial.print(" write ");
  Serial.println((uint32_t) wptr, 16);
#endif

  // check for buffer full

  if (tptr == rptr) {
    // buffer full, nothing to do
    return;
  }

  // buffer not full, compute next sample

  if (!drawingline) {
    setupline();
  }

  // everything is good to go to write a new sample, update write pointer
 
  wptr = tptr;

  // add new sample

#ifdef DEBUGV
  Serial.print("X ");
  Serial.print(drawx);
  Serial.print(" Y ");
  Serial.println(drawy);
#endif

  if (steep) {
    wptr->x = drawy;
    wptr->y = drawx; 
  } else {
    wptr->x = drawx;
    wptr->y = drawy;
  }

  // update variables for Bresenham calculation

  error2 += derror2;

  if (error2 > dx) {
    // time to update Y
    drawy += (drawy1 > drawy0) ? 1 : -1;
    error2 -= dx * 2;
  }

  // increment X, check for done

  if (++drawx > drawx1) {
    // end of line segment
    drawingline = false;
    pendown = false;
  }
}
