#if defined(__SAMD51__)
#define PERIODIC_TC         TC2
#define PERIODIC_TC_IRQn    TC2_IRQn
#define PERIODIC_TC_GCLK_ID TC2_GCLK_ID
#define PERIODIC_HANDLER    TC2_Handler
#define WAIT_TC16_REGS_SYNC(x) while (x->COUNT16.SYNCBUSY.bit.ENABLE) {}
#else
#define PERIODIC_TC         TC5
#define PERIODIC_TC_IRQn    TC2_IRQn
#define PERIODIC_HANDLER    TC5_Handler
#define WAIT_TC16_REGS_SYNC(x) while (x->COUNT16.STATUS.bit.SYNCBUSY) {}
#endif

#define PERIODIC_TC_CHANNEL 0
#define PERIODIC_TC_TOP     0xffff

#define MAXFREQ (F_CPU / 2) // max timer frequency

#define BUFSIZE 1024      // size of each waveform buffer (in samples)
#define HRES    (1 << 11) // half the resolution of the DAC

// buffer structure

struct sbuf {
  uint16_t    x;
  uint16_t    y;
};

// hook for periodic interrupt handler

void PERIODIC_HANDLER
  (void) __attribute__ ((weak, alias("Periodic")));

static bool           complained = false;
static double         phasex     = 0;
static double         phasey     = 0;
static uint32_t       sigma      = 0;
static uint32_t       nsamples   = 0;
static uint32_t       stall      = 0;
static struct sbuf     wave[BUFSIZE];
static struct sbuf *   wptr       = wave;
static struct sbuf *   rptr       = wave;
static struct sbuf *   bend       = wave + BUFSIZE;
static volatile bool  firstRun   = true;
static volatile bool  tick       = false;

// reset timer/counter

static inline void
resetTC(
  Tc *  TCx)
{
  // disable TCx
  TCx->COUNT16.CTRLA.reg &= ~TC_CTRLA_ENABLE;
  WAIT_TC16_REGS_SYNC(TCx);

  // reset TCx
  TCx->COUNT16.CTRLA.reg = TC_CTRLA_SWRST;
  WAIT_TC16_REGS_SYNC(TCx);

  while (TCx->COUNT16.CTRLA.bit.SWRST) {
  }
}

// set up a periodic interrupt

static void
setperiodic(
  uint32_t  frequency)
{
  uint8_t   factor;
  uint32_t  prescalerConfigBits;
  uint32_t  ccValue;

  NVIC_DisableIRQ(PERIODIC_TC_IRQn);
  NVIC_ClearPendingIRQ(PERIODIC_TC_IRQn);

  if (firstRun) {
    NVIC_SetPriority(PERIODIC_TC_IRQn, 0);
    firstRun = false;

#if defined(__SAMD51__)
    GCLK->PCHCTRL[PERIODIC_TC_GCLK_ID].reg =
      GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos);
#else
    GCLK->CLKCTRL.reg =
      (uint16_t) (GCLK_CLKCTRL_CLKEN |
                  GCLK_CLKCTRL_GEN_GCLK0 |
                  GCLK_CLKCTRL_ID(GCM_TC4_TC5));

    while (GCLK->STATUS.bit.SYNCBUSY) {
    }
#endif
  }

  ccValue = MAXFREQ / (frequency - 1);
  factor  = 0;

  //Serial.print("initial ccValue ");
  //Serial.println(ccValue);

  while (ccValue > PERIODIC_TC_TOP) {
    ccValue = (MAXFREQ / (frequency / (2 << factor))) - 1;
    //Serial.print("now ccValue ");
    //Serial.println(ccValue);
    ++factor;
    //Serial.print("factor ");
    //Serial.println(factor);

    switch (factor) {
      case 4: // DIV32  is not available
      case 6: // DIV128 is not available
      case 8: // DIV512 is not available
        ++factor;
    }
  }

  //Serial.print("final ccValue ");
  //Serial.println(ccValue);
  //Serial.print("final factor ");
  //Serial.println(factor);

  switch (factor - 1) {
    case 0:
      prescalerConfigBits = TC_CTRLA_PRESCALER_DIV2;
      break;

    case 1:
      prescalerConfigBits = TC_CTRLA_PRESCALER_DIV4;
      break;

    case 2:
      prescalerConfigBits = TC_CTRLA_PRESCALER_DIV8;
      break;

    case 3:
      prescalerConfigBits = TC_CTRLA_PRESCALER_DIV16;
      break;

    case 5:
      prescalerConfigBits = TC_CTRLA_PRESCALER_DIV64;
      break;

    case 7:
      prescalerConfigBits = TC_CTRLA_PRESCALER_DIV256;
      break;

    case 9:
      prescalerConfigBits = TC_CTRLA_PRESCALER_DIV1024;
      break;

    default:
      prescalerConfigBits = TC_CTRLA_PRESCALER_DIV1;
      break;
  }

  resetTC(PERIODIC_TC);

  // set timer counter mode to 16 bits
  prescalerConfigBits |= TC_CTRLA_MODE_COUNT16;

  // set PERIODIC_TC mode to match frequency
#if defined(__SAMD51__)
  PERIODIC_TC->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_MFRQ;
#else
  prescalerConfigBits |= TC_CTRLA_WAVEGEN_MFRQ
#endif

  //Serial.print("prescalerConfigBits ");
  //Serial.println(prescalerConfigBits, 16);

  PERIODIC_TC->COUNT16.CTRLA.reg |= prescalerConfigBits;

  WAIT_TC16_REGS_SYNC(PERIODIC_TC);

  PERIODIC_TC->COUNT16.CC[PERIODIC_TC_CHANNEL].reg = (uint16_t) ccValue;

  WAIT_TC16_REGS_SYNC(PERIODIC_TC);

  // enable PERIODIC_TC interrupt request

  PERIODIC_TC->COUNT16.INTENSET.bit.MC0 = 1;

  // enable PERIODIC_TC

  PERIODIC_TC->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;

  WAIT_TC16_REGS_SYNC(PERIODIC_TC);

  NVIC_EnableIRQ(PERIODIC_TC_IRQn);
}

extern "C" {

// this routine gets called periodically as an interrupt handler
// note that it should do anything time consuming

void
Periodic(void)
{
  // output next value to ADCs
  tick = true;
  // clear the interrupt
  PERIODIC_TC->COUNT16.INTFLAG.bit.MC0 = 1;
}

}

// called once at beginning of execution

void
setup()
{
  Serial.begin(9600);

  setperiodic(10000);
}

// called repeatedly during execution

void
loop()
{
  double        avgfull;
  uint16_t      x;
  uint16_t      y;
  uint32_t      full;
  struct sbuf *  tptr;

  if (tick) {
    // time to update signal, check if data available
    if (rptr != wptr) {
      // data is available, update signal
      analogWrite(A0, rptr->x);
      analogWrite(A1, rptr->y);

      // increment read pointer, check for wraparound
      if (++rptr == bend) {
        // read pointer at end of buffer, wrap around
        rptr = wave;
      }

      // serviced current tick
      tick = false;
    } else {
      // data not available, increment stall counter
      ++stall;
      complained = false;
    }
  }

  // check for stall wraparound

  if (((stall % 1000) == 0) && !complained) {
    Serial.print(stall);
    Serial.println(" stalls");
    complained = true;
  }

  // compute buffer fullness

  tptr = wptr + 1;

  if (tptr == bend) {
    tptr = wave;
  }

  if (tptr == rptr) {
    full = BUFSIZE;
  } else {
    if (tptr < rptr) {
      full = BUFSIZE - (rptr - tptr);
    } else {
      full = tptr - rptr;
    }
  }

  sigma += full;
  ++nsamples;

  if (nsamples == 100000) {
    avgfull = sigma / (double) nsamples;
    Serial.print("avg full ");
    Serial.print(avgfull);
    Serial.print(" (");
    Serial.print(100 * avgfull / BUFSIZE);
    Serial.println("%)");
    sigma    = 0;
    nsamples = 0;
  }

  // check for buffer full

  if (tptr == rptr) {
    // buffer full, we're done
    return;
  }

  // buffer not full, compute next sample

  wptr    = tptr;
  wptr->x = HRES * (1 + sin(phasex));
  wptr->y = HRES * (1 + sin(phasey));

  phasex += 0.02;
  phasey += 0.031;

  if (phasex > 2 * M_PI) {
    phasex -= 2 * M_PI;
  }

  if (phasey > 2 * M_PI) {
    phasey -= 2 * M_PI;
  }
}
