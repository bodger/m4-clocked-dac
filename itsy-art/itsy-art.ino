#include <Adafruit_ZeroTimer.h>

#define BUFSIZE           1024                // size of each waveform buffer (in samples)
#define DACRES            (1 << 12)           // DAC resolution
#define MAXDAC            (DACRES - 1)        // max DAC value
#define HRES              (DACRES >> 1)       // half the resolution of the DAC
#define MAXOBJECTS        3                   // max number of objects
#define MAXSCALE          800                 // max object scale
#define MAXSPEED          100                 // max speed ratio
#define MINEDGE           (MAXSCALE)
#define MAXEDGE           (DACRES - MAXSCALE)
#define POSRANGE          (DACRES - 2 * MAXSCALE)
#define SPEEDRANGE        (DACRES / MAXSPEED)
#define SPEEDOFF          (SPEEDRANGE / 2)
#define THETASPEEDRANGE   (TWO_PI / MAXSPEED)
#define THETASPEEDOFF     (THETASPEEDRANGE / 2)
#define RADIUSSPEEDRANGE  (MAXSCALE / MAXSPEED)
#define RADIUSSPEEDOFF    (RADIUSSPEEDRANGE / 2)
#define UPDATEINTERVAL    100

#define SWAP(x, y)          tmp = x; x = y; y = tmp

//#define DEBUGV
//#define DEBUGXY
//#define DEBUGSET
//#define DEBUGSHAPE
//#define DEBUGSETUP

#ifdef DEBUGV || DEBUGXY || DEBUGSET || DEBUGSHAPE || DEBUGSETUP
#define DEBUG
#endif

//#define DEBUG

// coördinate buffer structure

struct sbuf {
  uint16_t    x;
  uint16_t    y;
};

// object position structure (X and Y)

struct pos {
  int         x;
  int         y;
};

// polar vertex structure (angle and magnitude)

struct vertex {
  float       theta;
  float       radius;
};

// line segment structure (pair of vertices)

struct segment {
  struct vertex vertices[2];
};

// symbol drawing structure (set of line segments)

struct symbol {
  int               nsegments;    // number of segments
  struct segment *  segments;     // segments
};

// regular figure description structure (size, number of points, connection scheme)

struct figure {
  float radius;
  int   nangles;
  int   nvertices;
  int   increment;
  int   next;
};

// object parameters (position, size, rotation)

struct param {
  struct pos      pos;
  float           theta;
  float           radius;
};

// display object structure (symbol, current segment, current position, size, rotation)

struct object {
  struct symbol *   symbolp;
  struct segment *  segmentp;
  struct segment *  segmente;
  struct param      curparam;
  struct param      dparam;
};

// forward declarations

static void           initshapes();
static void           initobjects();
static void           setupline();
static void           updateobjects();
static void           getnextobjectsegment();
static void           setupobject(struct symbol * symbolp);
static void           buildfromvertices(const struct figure * figurep, const struct symbol * symbolp);
static float          frandom(float range);

// allocate segment storage for symbols

static segment        squaresegments[4];
static segment        starsegments[5];
static segment        asterisksegments[3];

// symbol storage

static struct symbol  square    = { 4, squaresegments };
static struct symbol  star      = { 5, starsegments };
static struct symbol  asterisk  = { 3, asterisksegments };

// regular figure definitions

static struct figure  squaref   = { 0.5, 4, 4, 1, 1 };
static struct figure  starf     = { 1.0, 5, 5, 2, 2 };
static struct figure  asteriskf = { 0.7, 6, 3, 1, 3 };

// drawing object storage

static struct object  objects[MAXOBJECTS];

// global variables

static int                  freq;                   // current sample frequency
static int                  drawx0;                 // line segment begin X
static int                  drawy0;                 // line segment begin Y
static int                  drawx1;                 // line segment end X
static int                  drawy1;                 // line segment end Y
static int                  dx;                     // delta X
static int                  dy;                     // delta Y
static int                  drawx;                  // current point X
static int                  drawy;                  // current point Y
static int                  error2;                 // error term
static int                  derror2;                // delta error term
static bool                 steep;                  // line segment slope > 1 flag
static bool                 drawingline = false;    // currently drawing a line segment flag
static float                scale;                  // current scale factor
static struct sbuf          wave[BUFSIZE];          // output wave circular buffer
static struct sbuf *        wptr = wave;            // output write pointer
static struct sbuf *        rptr = wave;            // output read pointer
static struct sbuf *        bend = wave + BUFSIZE;  // output buffer end pointer
static struct object *      objectp;                // current object pointer
static struct object *      objecte;                // current object end
static volatile bool        tick = false;           // output clock tick flag
static unsigned long        nextupdate;             // update objects time
static Adafruit_ZeroTimer   zerotimer = Adafruit_ZeroTimer(3);

// set up AdaFruit ZeroTimer handler

void TC3_Handler() {
  Adafruit_ZeroTimer::timerHandler(3);
}

// timer callback, just set flag

void TimerCallback0(void)
{
  tick = true;
}

// update timer frequency

static void updatefreq()
{  
  zerotimer.enable(false);
  zerotimer.setCompare(0, freq);
  zerotimer.setCallback(true, TC_CALLBACK_CC_CHANNEL0, TimerCallback0);
  zerotimer.enable(true);
}

static float
frandom(
  float range)
{
    return ((random() % 10000) * range) / 10000;
}

// check inputs to see if user wants to adjust the settings

static void checkinputs()
{
  int             newfreq;  // new sample frequency
  float           newscale; // new scale factor

  newfreq  = analogRead(A5) / 32 + 48;
  newscale = analogRead(A4) / 64.0 + 1;

#ifdef DEBUGSETV
  Serial.print("newfreq ");
  Serial.print(newfreq);
  Serial.print(" newscale ");
  Serial.println(newscale);
#endif

  if (abs(newfreq - freq) > 0x2) {
    // new frequency detected, update timer
#ifdef DEBUGSET
    Serial.print("new frequency ");
    Serial.println(newfreq);
#endif
    freq = newfreq;
    updatefreq();
  }

  if (abs(newscale - scale) > 1) {
#ifdef DEBUGSET
    Serial.print("new scale ");
    Serial.println(newscale);
#endif
    if (newscale != 0) {
      scale = newscale;
    }
  }
}

// update objects

static void
updateobjects()
{
  unsigned long curtime;

  curtime = millis();

  if (curtime > nextupdate) {
    for (objectp = objects; objectp < objecte; ++objectp) {
      objectp->curparam.pos.x  += objectp->dparam.pos.x;
      objectp->curparam.pos.y  += objectp->dparam.pos.y;
      objectp->curparam.theta  += objectp->dparam.theta;
      objectp->curparam.radius += objectp->dparam.radius;
    }

    objectp    = objects;
    nextupdate = curtime + UPDATEINTERVAL;
  }
}

// get next line segment to draw

static void
getnextobjectsegment()
{
  ++objectp->segmentp;

  if (objectp->segmentp >= objectp->segmente) {
    ++objectp;

    if (objectp >= objecte) {
      objectp = objects;
      updateobjects();
      checkinputs();
    }

    objectp->segmentp = objectp->symbolp->segments;
  }

#ifdef DEBUGSHAPE
  Serial.print("object ");
  Serial.print(objectp - objects);
  Serial.print(" segment ");
  Serial.println(objectp->segmentp - objectp->symbolp->segments);
#endif
}

// set up line drawing equation

static void
setupline()
{
  int               tmp;      // temporary variable used for swapping
  float             radius;
  float             theta;
  struct param *    cp;
  struct vertex *   vp;

  getnextobjectsegment();

  cp = &(objectp->curparam);        // get pointer to current object parameters
  vp = objectp->segmentp->vertices; // get pointer to current segment start

  // compute endpoints in drawing space

  radius = vp->radius * cp->radius;
  theta  = vp->theta + cp->theta;
  drawx0 = (radius * sin(theta) + cp->pos.x) / scale;
  drawy0 = (radius * cos(theta) + cp->pos.y) / scale;

  ++vp;                             // increment vertex pointer to segment end

  radius = vp->radius * cp->radius;
  theta  = vp->theta + cp->theta;
  drawx1 = (radius * sin(theta) + cp->pos.x) / scale;
  drawy1 = (radius * cos(theta) + cp->pos.y) / scale;

#ifdef DEBUGXY
  Serial.print(drawx0);
  Serial.print(",");
  Serial.print(drawy0);
  Serial.print(" - ");
  Serial.print(drawx1);
  Serial.print(",");
  Serial.println(drawy1);
#endif
  drawingline = true;
  steep       = false;

  if (abs(drawx0 - drawx1) < abs(drawy0 - drawy1)) {
    // slope more than 1, increment Y instead of X
    SWAP(drawx0, drawy0);
    SWAP(drawx1, drawy1);
    steep = true;
  }

  if (drawx0 > drawx1) {
    // counting down, swap points to count up
    SWAP(drawx0, drawx1);
    SWAP(drawy0, drawy1);
  }

  dx      = drawx1 - drawx0;
  dy      = drawy1 - drawy0;
#ifdef DEBUGV
  Serial.print("dx ");
  Serial.print(dx);
  Serial.print(" dy ");
  Serial.println(dy);
#endif
  derror2 = abs(dy) * 2;
  error2  = 0;
  drawx   = drawx0;
  drawy   = drawy0;
}

// build regular figure segments from vertex description

static void
buildfromvertices(
  const struct figure *   figurep,
  const struct symbol *   symbolp)
{
  int                     segment;
  float                   arc;
  float                   increment;
  float                   theta;
  float                   endtheta;
  struct segment *        segmentp;

  arc       = TWO_PI / figurep->nangles;
  increment = arc * figurep->increment;
  endtheta  = increment * figurep->nvertices;
  segmentp  = symbolp->segments;
#ifdef DEBUGSETUP
  Serial.print("arc ");
  Serial.print(arc * RAD_TO_DEG);
  Serial.print(" increment ");
  Serial.print(figurep->increment);
  Serial.print(" next ");
  Serial.print(figurep->next);
  Serial.print(" endtheta ");
  Serial.println(endtheta * RAD_TO_DEG);
#endif

  theta = 0;

  for (segment = 0; segment < figurep->nvertices; ++segment) {
    segmentp->vertices[0].theta = theta;
    segmentp->vertices[1].theta = theta + arc * figurep->next;
    segmentp->vertices[0].radius = figurep->radius;
    segmentp->vertices[1].radius = figurep->radius;
#ifdef DEBUGSETUP
    Serial.print("segment theta ");
    Serial.print(segmentp->vertices[0].theta * RAD_TO_DEG);
    Serial.print(" - ");
    Serial.print(segmentp->vertices[1].theta * RAD_TO_DEG);
    Serial.print(" radius ");
    Serial.println(segmentp->vertices[0].radius);
#endif
    ++segmentp;
    theta += increment;
  }
}

// initialize shapes

static void
initshapes()
{
#ifdef DEBUGSETUP
  Serial.println("square:");
#endif
  buildfromvertices(&squaref,   &square);

#ifdef DEBUGSETUP
  Serial.println("star:");
#endif
  buildfromvertices(&starf,     &star);

#ifdef DEBUGSETUP
  Serial.println("asterisk:");
#endif
  buildfromvertices(&asteriskf, &asterisk);
}

// set up an object, choose parameters and change rate

static void
setupobject(
  struct symbol * symbolp)
{
  objectp->symbolp = symbolp;
  objectp->segmentp = symbolp->segments;
  objectp->segmente = symbolp->segments + symbolp->nsegments;
  objectp->curparam.pos.x  = random() % POSRANGE + MINEDGE;
  objectp->curparam.pos.y  = random() % POSRANGE + MINEDGE;
  objectp->curparam.theta  = frandom(TWO_PI);
  objectp->curparam.radius = random() % MAXSCALE;
  objectp->dparam.pos.x    = random() % SPEEDRANGE - SPEEDOFF;
  objectp->dparam.pos.y    = random() % SPEEDRANGE - SPEEDOFF;
  objectp->dparam.theta    = frandom(THETASPEEDRANGE) - THETASPEEDOFF;
  objectp->dparam.radius   = random() % RADIUSSPEEDRANGE - RADIUSSPEEDOFF;

#ifdef DEBUGSETUP
  Serial.print("object segments ");
  Serial.print(objectp->segmente - objectp->segmentp);
  Serial.print(" (");
  Serial.print(objectp->curparam.pos.x);
  Serial.print(", ");
  Serial.print(objectp->curparam.pos.y);
  Serial.print(") theta ");
  Serial.print(objectp->curparam.theta * RAD_TO_DEG);
  Serial.print(" radius ");
  Serial.print(objectp->curparam.radius);
  Serial.print(" delta (");
  Serial.print(objectp->dparam.pos.x);
  Serial.print(", ");
  Serial.print(objectp->dparam.pos.y);
  Serial.print(") theta ");
  Serial.print(objectp->dparam.theta);
  Serial.print(" radius ");
  Serial.println(objectp->dparam.radius);
#endif

  ++objectp;
}

// initialize drawing objects and pointers

static void
initobjects()
{
  objectp = objects;

  setupobject(&star);
  setupobject(&square);
  setupobject(&asterisk);

  objecte = objectp;
  objectp = objects;
}

// called once at beginning of execution

void
setup()
{
#ifdef DEBUG
  uint8_t i;
#endif

  srandom(analogRead(A2));

#ifdef DEBUG
  while (!Serial) {
  }

  Serial.begin(9600);

  for (i = 0; i < 5; ++i) {
    Serial.println(i);

    delay(200);
  }

  Serial.println("init()");
#endif

  initshapes();
  initobjects();

  freq  = 200;
  scale = 2;

  zerotimer.enable(false);
  zerotimer.configure(TC_CLOCK_PRESCALER_DIV1,       // prescaler
          TC_COUNTER_SIZE_16BIT,       // bit width of timer/counter
          TC_WAVE_GENERATION_MATCH_PWM // frequency or PWM mode
          );

  zerotimer.setCompare(0, freq);
  zerotimer.setCallback(true, TC_CALLBACK_CC_CHANNEL0, TimerCallback0);
  zerotimer.enable(true);
}

// called repeatedly during execution

void
loop()
{
  struct sbuf *   tptr;     // temporary write pointer

  if (tick) {
    // time to update signal, check if data available
    if (rptr != wptr) {
      // data is available, update signal
      analogWrite(A0, rptr->x * scale);
      analogWrite(A1, rptr->y * scale);

      // increment read pointer, check for wraparound
      if (++rptr == bend) {
        // read pointer at end of buffer, wrap around
        rptr = wave;
      }

      // serviced current tick
      tick = false;
    }
  }

  // make temporary copy of new write pointer

  tptr = wptr + 1;

  if (tptr == bend) {
    tptr = wave;
  }

#ifdef DEBUGV
  Serial.print("read ");
  Serial.print((uint32_t) rptr, 16);
  Serial.print(" write ");
  Serial.println((uint32_t) wptr, 16);
#endif

  // check for buffer full

  if (tptr == rptr) {
    // buffer full, nothing to do
    return;
  }

  // buffer not full, compute next sample

  if (!drawingline) {
    setupline();
  }

  // everything is good to go to write a new sample, update write pointer
 
  wptr = tptr;

  // add new sample

#ifdef DEBUGV
  Serial.print("X ");
  Serial.print(drawx);
  Serial.print(" Y ");
  Serial.println(drawy);
#endif

  if (steep) {
    wptr->x = drawy;
    wptr->y = drawx; 
  } else {
    wptr->x = drawx;
    wptr->y = drawy;
  }

  // update variables for Bresenham calculation

  error2 += derror2;

  if (error2 > dx) {
    // time to update Y
    drawy += (drawy1 > drawy0) ? 1 : -1;
    error2 -= dx * 2;
  }

  // increment X, check for done

  if (++drawx > drawx1) {
    // end of line segment
    drawingline = false;
  }
}
